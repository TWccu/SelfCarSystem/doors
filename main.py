import json
import socket
import time

import paho.mqtt.client as mqtt

from led_controller import SevenSegmentDisplay
from motor_controller import CCUPiMotor

mc = CCUPiMotor(13, 11, 22, 32)  # R(13,11) is sevenSegmentDisplay[0]; L(18,16) is sevenSegmentDisplay[1]
sevenSegmentDisplay = [SevenSegmentDisplay(29, 31, 33), SevenSegmentDisplay(36, 38, 40)]

# 載入設定檔
with open('selfCarConfig.json') as f:
    data = json.load(f)
    # 訂閱的topic (應為control center送出來的topic)
    mqtt_in_topic = data["mqtt_in_topic"]
    print("mqtt_in_topic: ", mqtt_in_topic)
    # 輸出的topic (應為輸入control center的topic)
    mqtt_out_topic = data["mqtt_out_topic"]
    print("mqtt_out_topic: ", mqtt_out_topic)
    # # mqtt的server
    # mqtt_broker = data["mqtt_broker"]
    # print("mqtt_broker: ", mqtt_broker)
    # # mqtt的port
    # mqtt_port = data["mqtt_port"]
    # print("mqtt_port: ", mqtt_port)
    # 閘門開啟(上捲)速度
    up_scroll_second = data["up_scroll_second"]
    print("up_scroll_second: ", up_scroll_second)
    # 閘門開啟(上捲)持續時間
    up_scroll_speed = data["up_scroll_speed"]
    print("up_scroll_speed: ", up_scroll_speed)
    # 閘門關閉(下捲)速度
    down_scroll_second = data["down_scroll_second"]
    print("down_scroll_second: ", down_scroll_second)
    # 閘門關閉(下捲)持續時間
    down_scroll_speed = data["down_scroll_speed"]
    print("down_scroll_speed: ", down_scroll_speed)

client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
client.bind(("", 23130))
data, addr = client.recvfrom(1024)
payload = json.loads(data.decode('utf-8'))
mqtt_broker = payload['mqttip']
mqtt_port = payload['mqttport']
print('MQTT Server ' + mqtt_broker + ':' + str(mqtt_port))


# 設定閘門上的七段顯示器
def setSevenSegmentDisplay(doorNum, num):
    sevenSegmentDisplay[doorNum - 1].display(num)

# 連上Mqtt後第一時間的動作
def on_connect(client, userData, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe(mqtt_in_topic)
    time.sleep(1)
    # 詢問1號閘門的數字
    publish_mqtt("state,1")
    time.sleep(1)
    # 詢問2號閘門的數字
    publish_mqtt("state,2")

def spilt_message(msgs):
    return msgs.split(',')

# 發布mqtt訊息的method
def publish_mqtt(msgs):
    client.publish(mqtt_out_topic, msgs)

# 開啟閘門的method
def openDoor(doorNum):
    if doorNum == 1:
        mc.setSpeed(up_scroll_speed, 0)
    elif doorNum == 2:
        mc.setSpeed(0, up_scroll_speed)
    time.sleep(up_scroll_second)
    mc.stop()

# 關閉閘門的method
def closeDoor(doorNum):
    if doorNum == 1:
        mc.setSpeed(-down_scroll_speed, 0)
    elif doorNum == 2:
        mc.setSpeed(0, -down_scroll_speed)
    time.sleep(down_scroll_second)
    mc.stop()

# 當收到mqtt的訊息時所需做的事情
def on_message(client, userData, msg):
    msgs = msg.payload.decode("utf8")
    print(msgs)
    msgs_array = spilt_message(msgs)
    # 設定閘門數字 格式：doorNum,Num
    if msgs_array[0] == str(1) or msgs_array[0] == str(2):
        setSevenSegmentDisplay(int(msgs_array[0]), int(msgs_array[1]))
    # 打開閘門 格式：open,doorNum
    elif msgs_array[0] == "open":
        openDoor(int(msgs_array[1]))
    # 關閉閘門 格式：close,doorNum
    elif msgs_array[0] == "close":
        closeDoor(int(msgs_array[1]))


if __name__ == '__main__':
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(mqtt_broker, mqtt_port, 60)
    client.loop_forever()
