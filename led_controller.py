import RPi.GPIO as GPIO
import time

class SevenSegmentDisplay:

    segCode = [0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f]

    def __init__(self, ds, stcp, shcp):
        self.shcp = shcp
        self.stcp = stcp
        self.ds = ds
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.ds, GPIO.OUT)
        GPIO.setup(self.stcp, GPIO.OUT)
        GPIO.setup(self.shcp, GPIO.OUT)
        GPIO.output(self.ds, GPIO.LOW)
        GPIO.output(self.stcp, GPIO.LOW)
        GPIO.output(self.shcp, GPIO.LOW)

    def hc595_shift(self, dat):
        for bit in range(0, 8):
            GPIO.output(self.ds, 0x80 & (dat << bit))
            GPIO.output(self.shcp, GPIO.HIGH)
            time.sleep(0.001)
            GPIO.output(self.shcp, GPIO.LOW)
        print("do something")
        GPIO.output(self.stcp, GPIO.HIGH)
        time.sleep(0.001)
        GPIO.output(self.stcp, GPIO.LOW)

    def display(self, num):
        if 9 >= num >= 0:
            self.hc595_shift(self.segCode[num])
        else:
            raise Exception('just allow 0 - 9')

    def __delete__(self):
        GPIO.cleanup()